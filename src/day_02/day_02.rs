use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::Result;

use super::game::Game;

struct Letters {
    pub your: String,
    pub their: String,
}

fn parse_line(line: String) -> Result<Letters> {
    let letters: Vec<&str> = line.split(" ").collect();
    let theirs = letters[0];
    let yours = letters[1];
    return Ok(Letters{
        your: yours.to_string(),
        their: theirs.to_string()
    })
}

pub fn day_02(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut score: i32 = 0;

    for line in reader.lines().into_iter() {
        let l = line?;
        let tuple = parse_line(l)?;
        score += Game::calculate_score(tuple.your, tuple.their)?;
    }

    Ok(score.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day_02() {
        let res = day_02("./src/day_02/test-input.txt".to_string());
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), "12");
    }
}
