use anyhow::{anyhow, Result};

#[derive(PartialEq, Eq)]
pub enum Game {
    Rock,
    Paper,
    Scissors,
}

const LETTERS: &'static [&str] = &["A", "B", "C", "X", "Y", "Z"];

impl Game {
    fn from_string(letter: &str) -> Result<Game> {
        if !LETTERS.contains(&letter) {
            return Err(anyhow!("The letter {} is not correct", letter));
        }

        match letter {
            "A" => Ok(Game::Rock),
            "B" => Ok(Game::Paper),
            "C" => Ok(Game::Scissors),
            _ => Err(anyhow!("Unknown letter {}", letter)),
        }
    }

    fn from_game(g: &Game) -> Game {
        match g {
            Game::Rock => Game::Rock,
            Game::Paper => Game::Paper,
            Game::Scissors => Game::Scissors,
        }
    }

    fn get_score(&self) -> i32 {
        match self {
            Game::Rock => 1,
            Game::Paper => 2,
            Game::Scissors => 3,
        }
    }

    fn win_score(yours: &Game, theirs: &Game) -> i32 {
        if yours == theirs {
            return 3;
        }
        match yours {
            Game::Rock => {
                if theirs == &Game::Scissors {
                    6
                } else {
                    0
                }
            }
            Game::Paper => {
                if theirs == &Game::Rock {
                    6
                } else {
                    0
                }
            }
            Game::Scissors => {
                if theirs == &Game::Paper {
                    6
                } else {
                    0
                }
            }
        }
    }

    fn total_score(yours: &Game, theirs: &Game) -> i32 {
        yours.get_score() + Game::win_score(yours, theirs)
    }

    pub fn calculate_score(yours: String, theirs: String) -> Result<i32> {
        // Easy
        // let your_choice = Game::from_string(&yours)?;
        // let their_choice = Game::from_string(&theirs)?;
        // Hard
        let their_choice = Game::from_string(&theirs)?;
        let your_choice = Game::choice(&their_choice, yours);
        Ok(Game::total_score(&your_choice, &their_choice))
    }

    fn choice(their: &Game, letter: String) -> Game {
        if letter == "Y" {
            Game::from_game(their)
        }
        // Need to lose
        else if letter == "X" {
            match their {
                Game::Rock => Game::Scissors,
                Game::Paper => Game::Rock,
                Game::Scissors => Game::Paper,
            }
        } else {
            match their {
                Game::Rock => Game::Paper,
                Game::Paper => Game::Scissors,
                Game::Scissors => Game::Rock,
            }
        }
    }
}
