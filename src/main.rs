use advent_of_code_rust_2022::{files::get_file_path, runner};
use clap::Parser;
use termion::{color, style};

extern crate advent_of_code_rust_2022;

#[derive(Parser, Debug)]
#[command(author, version)]
struct Args {
    #[arg(short, long)]
    test: bool,

    #[arg(short, long)]
    day: i32,
}

fn get_test_mode_str(is_test: bool) -> String {
    match is_test {
        true => String::from("ON"),
        false => String::from("OFF"),
    }
}

fn print_summary(day: i32, is_test: bool) {
    println!(
        "===== {}SUMMARY{} =====\n",
        color::Fg(color::Blue),
        style::Reset
    );
    println!(
        "    Day      : {}",
        format!("{}{}{}", color::Fg(color::Blue), day, style::Reset)
    );
    println!(
        "    Test mode: {}",
        format!(
            "{}{}{}",
            color::Fg(color::Blue),
            get_test_mode_str(is_test),
            style::Reset
        )
    );
}

fn run(day: i32, fpath: String) {
    match runner::run(day, fpath) {
        Ok(res) => println!("\nResult: {}", res),
        Err(err) => eprintln!("Error: {}", err),
    }
}

fn main() {
    let args = Args::parse();
    print_summary(args.day, args.test);
    match get_file_path(args.day, args.test) {
        Ok(fpath) => run(args.day, fpath),
        Err(err) => eprintln!("Error: {}", err),
    }
}
