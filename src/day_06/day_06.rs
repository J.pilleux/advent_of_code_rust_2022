use std::{fs, collections::HashSet};

use anyhow::Result;

pub fn day_06(fpath: String) -> Result<String> {
    let data = fs::read_to_string(fpath).expect("Unable to read file");

    let mut res = 0;

    for i in 14..data.len() {
        let tmp = i - 14;
        let s = &data[tmp..i];
        let mut uniq = HashSet::new();
        let has_dupes = s.chars().into_iter().all(move |x| uniq.insert(x));
        if has_dupes {
            res = i;
            break;
        }
    }


    Ok(res.to_string())
}
