use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::Result;

struct Trees {
    all_trees: Box<Vec<Vec<u32>>>,
}

impl Trees {
    fn new() -> Trees {
        Trees {
            all_trees: Box::new(Vec::new()),
        }
    }

    fn populate(&mut self, fpath: String) -> Result<()> {
        let file = File::open(&fpath)?;
        let reader = BufReader::new(file);

        for line in reader.lines().into_iter() {
            let l = line?;
            let mut row: Vec<u32> = Vec::new();
            for c in l.chars() {
                if !c.is_numeric() {
                    continue;
                }
                row.push(c.to_digit(10).unwrap());
            }
            self.all_trees.push(row);
        }

        Ok(())
    }

    fn get_visible_trees(&self) -> Result<u32> {
        let mut res: u32 = 0;

        for i in 0..self.all_trees.len() {
            for j in 0..self.all_trees[i].len() {
                // All trees in edges are visible
                if i == 0
                    || j == 0
                    || i == self.all_trees.len() - 1
                    || j == self.all_trees[i].len() - 1
                {
                    res += 1;
                    continue;
                }

                if self.is_visible(i, j)? {
                    res += 1;
                    continue;
                }
            }
        }

        Ok(res)
    }

    fn get_best_tree_score(&self) -> Result<u32> {
        let mut max_score: u32 = 0;

        for i in 0..self.all_trees.len() {
            for j in 0..self.all_trees[i].len() {
                let cur_score = self.tree_score(i, j)?;
                if cur_score > max_score {
                    max_score = cur_score;
                }
            }
        }

        Ok(max_score)
    }

    fn is_visible(&self, i: usize, j: usize) -> Result<bool> {
        let tree_val = self.all_trees[i][j];
        let mut visible = vec![true, true, true, true];

        for ii in 0..i {
            let cur = self.all_trees[ii][j];
            if cur >= tree_val {
                visible[0] = false;
            }
        }

        for ii in i + 1..self.all_trees.len() {
            let cur = self.all_trees[ii][j];
            if cur >= tree_val {
                visible[1] = false;
            }
        }

        for jj in 0..j {
            let cur = self.all_trees[i][jj];
            if cur >= tree_val {
                visible[2] = false;
            }
        }

        for jj in j + 1..self.all_trees[i].len() {
            let cur = self.all_trees[i][jj];
            if cur >= tree_val {
                visible[3] = false;
            }
        }

        let res: bool = visible.iter().fold(false, |acc, cur| acc || *cur);
        Ok(res)
    }

    fn tree_score(&self, i: usize, j: usize) -> Result<u32> {
        let tree_val = self.all_trees[i][j];
        let mut scores: Vec<u32> = vec![0, 0, 0, 0];

        for ii in (0..i).rev() {
            let cur = self.all_trees[ii][j];
            scores[0] += 1;
            if cur >= tree_val {
                break;
            }
        }

        for ii in i..self.all_trees.len() {
            let cur = self.all_trees[ii][j];
            scores[1] += 1;
            if cur >= tree_val {
                break;
            }
        }

        for jj in (0..j).rev() {
            let cur = self.all_trees[i][jj];
            scores[2] += 1;
            if cur >= tree_val {
                break;
            }
        }

        for jj in j + 1..self.all_trees[i].len() {
            let cur = self.all_trees[i][jj];
            scores[3] += 1;
            if cur >= tree_val {
                break;
            }
        }

        let f = |acc: u32, n: &u32| -> u32 {
            if n > &0 {
                acc * n
            } else {
                acc
            }
        };

        let final_score: u32 = scores.iter().fold(1, f);

        Ok(final_score)
    }
}

pub fn day_08(fpath: String) -> Result<String> {
    let mut trees = Trees::new();
    trees.populate(fpath)?;

    Ok(trees.get_best_tree_score()?.to_string())
    // trees.tree_score(3, 2);
    // Ok(trees.get_visible_trees()?.to_string())
    // Ok("".to_string())
}
