use anyhow::{anyhow, Result};

use crate::{
    day_01::day_01::day_01, day_02::day_02::day_02, day_03::day_03::day_03, day_04::day_04::day_04,
    day_05::day_05::day_05, day_06::day_06::day_06, day_07::day_07::day_07, day_08::day_08::day_08,
};

pub fn run(day: i32, fpath: String) -> Result<String> {
    match day {
        1 => day_01(fpath),
        2 => day_02(fpath),
        3 => day_03(fpath),
        4 => day_04(fpath),
        5 => day_05(fpath),
        6 => day_06(fpath),
        7 => day_07(fpath),
        8 => day_08(fpath),
        9 => Err(anyhow!("Day {} not implemented (yet)", day)),
        10 => Err(anyhow!("Day {} not implemented (yet)", day)),
        11 => Err(anyhow!("Day {} not implemented (yet)", day)),
        12 => Err(anyhow!("Day {} not implemented (yet)", day)),
        13 => Err(anyhow!("Day {} not implemented (yet)", day)),
        14 => Err(anyhow!("Day {} not implemented (yet)", day)),
        15 => Err(anyhow!("Day {} not implemented (yet)", day)),
        16 => Err(anyhow!("Day {} not implemented (yet)", day)),
        17 => Err(anyhow!("Day {} not implemented (yet)", day)),
        18 => Err(anyhow!("Day {} not implemented (yet)", day)),
        19 => Err(anyhow!("Day {} not implemented (yet)", day)),
        20 => Err(anyhow!("Day {} not implemented (yet)", day)),
        21 => Err(anyhow!("Day {} not implemented (yet)", day)),
        22 => Err(anyhow!("Day {} not implemented (yet)", day)),
        23 => Err(anyhow!("Day {} not implemented (yet)", day)),
        24 => Err(anyhow!("Day {} not implemented (yet)", day)),
        25 => Err(anyhow!("Day {} not implemented (yet)", day)),
        26 => Err(anyhow!("Day {} not implemented (yet)", day)),
        27 => Err(anyhow!("Day {} not implemented (yet)", day)),
        28 => Err(anyhow!("Day {} not implemented (yet)", day)),
        29 => Err(anyhow!("Day {} not implemented (yet)", day)),
        30 => Err(anyhow!("Day {} not implemented (yet)", day)),
        31 => Err(anyhow!("Day {} not implemented (yet)", day)),
        _ => Err(anyhow!("Unvalid day: {}", day)),
    }
}
