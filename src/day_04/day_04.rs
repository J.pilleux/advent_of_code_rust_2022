use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::Result;

#[derive(Debug)]
struct Pairs {
    pub fst: i32,
    pub sec: i32,
}

pub fn day_04(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut count: i32 = 0;

    for line in reader.lines().into_iter() {
        let l = line?;

        let pair: Vec<&str> = l.split(",").collect();
        let first: Vec<&str> = pair[0].split("-").collect();
        let second: Vec<&str> = pair[1].split("-").collect();

        let fst = Pairs { fst:  first[0].parse::<i32>()?, sec: first[1].parse::<i32>()?};
        let sec = Pairs { fst:  second[0].parse::<i32>()?, sec: second[1].parse::<i32>()?};

        if sec.fst >= fst.fst && sec.fst <= fst.sec {
            count += 1;
        } else if fst.fst >= sec.fst && fst.fst <= sec.sec {
            count += 1;
        }
    }

    Ok(count.to_string())
}
