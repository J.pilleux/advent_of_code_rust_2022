use std::fs::File;
use std::io::{BufRead, BufReader, Lines};

use anyhow::{Result, anyhow};

#[derive(Debug)]
struct Stacks {
    pub stacks: Vec<Vec<String>>
}

impl Stacks {
    pub fn new() -> Stacks {
        let mut v: Vec<Vec<String>> = Vec::new();
        for _ in 0..9 {
            v.push(Vec::new())
        }

        Stacks { stacks: v }
    }

    pub fn rev(&mut self) {
        for v in self.stacks.iter_mut() {
            v.reverse();
        }
    }

    #[allow(dead_code)]
    pub fn move_letter(&mut self, nb: i32, from: usize, to: usize) {
        for _ in 0..nb {
            let last = self.stacks[from - 1].pop().unwrap();
            self.stacks[to - 1].push(last);
        }
    }

    pub fn move_keep_order(&mut self, nb: i32, from: usize, to: usize) {
        let mut buff: Vec<String> = Vec::new();
        for _ in 0..nb {
            let last = self.stacks[from - 1].pop().unwrap();
            buff.push(last);
        }

        buff.reverse();

        for c in buff {
            self.stacks[to - 1].push(String::from(c));
        }
    }

    pub fn last_of_each(&self) -> String {
        let mut res: Vec<String> = Vec::new();
        for v in &self.stacks {
            if let Some(c) = v.last() {
                res.push(String::from(c));
            }
        }

        res.join("")
    }
}

fn get_stacks_txt(it: &mut Lines<BufReader<File>>) -> Result<Stacks> {
    let mut stacks = Stacks::new();

    while let Some(line) = it.next() {
        let l = match line {
            Ok(li) => li,
            Err(err) => return Err(anyhow!("{}", err))
        };

        if l == "" {
            break;
        }

        for (i, c) in l.chars().enumerate() {
            if c.is_alphabetic() {
                let index = i / 4;
                stacks.stacks[index].push(String::from(c))
            }
        }
    }

    stacks.rev();
    Ok(stacks)
}

pub fn day_05(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut it = reader.lines().into_iter();
    let mut stacks = get_stacks_txt(&mut it)?;

    while let Some(line) = it.next() {
        let l = line?;
        let elts: Vec<&str> = l.split(" ").collect();
        let nb = elts[1].parse::<i32>()?;
        let from = elts[3].parse::<usize>()?;
        let to = elts[5].parse::<usize>()?;
        stacks.move_keep_order(nb, from, to);
    }


    Ok(stacks.last_of_each())
}
