use std::cell::RefCell;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use anyhow::Result;

#[derive(Debug)]
struct FileNode {
    size: i32,
}

impl FileNode {
    fn new(size: i32) -> FileNode {
        FileNode { size }
    }
}

#[derive(Debug)]
struct Directory {
    father: Option<Rc<RefCell<Directory>>>,
    dirs: Vec<Rc<RefCell<Directory>>>,
    files: Vec<Rc<RefCell<FileNode>>>,
}

impl Directory {
    fn new(father: Option<Rc<RefCell<Directory>>>) -> Directory {
        Directory {
            father,
            dirs: Vec::new(),
            files: Vec::new(),
        }
    }

    fn add_file(&mut self, size: i32) {
        let file_ref = Rc::new(RefCell::new(FileNode::new(size)));
        self.files.push(file_ref);
    }

    fn get_dir_size(&self) -> i32 {
        let self_size = self.files.iter().fold(0, |acc, f| acc + f.borrow().size);
        if self.dirs.len() == 0 {
            return self_size;
        }

        let subdirs_size = self.dirs.iter().fold(0, |acc, d| acc + d.borrow().get_dir_size());
        return subdirs_size + self_size;
    }

    fn get_all_size(&self) -> Vec<i32> {
        let self_size = self.get_dir_size();
        let mut sizes = self.dirs.iter().fold(Vec::new(), |mut acc, d| {
            acc.extend(d.borrow().get_all_size());
            acc
        });
        sizes.push(self_size);
        return sizes;
    }
}

pub fn day_07(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut it = reader.lines().into_iter();
    it.next(); // Skiping the "cd /"

    let root = Rc::new(RefCell::new(Directory::new(None)));
    let mut current_node = Rc::clone(&root);

    while let Some(line) = it.next() {
        let l = line?;

        // The line is a file
        if l.as_bytes()[0].is_ascii_digit() {
            let file_size = l.split(" ").nth(0).unwrap().parse::<i32>()?;
            current_node.borrow_mut().add_file(file_size);
        } else if l.starts_with("$ cd") {
            let dest = l.split(" ").nth(2).unwrap();
            if dest == ".." {
                let current_clone = Rc::clone(&current_node);
                current_node = Rc::clone(current_clone.borrow().father.as_ref().unwrap());
            } else {
                let new_directory = Rc::new(RefCell::new(Directory::new(None)));
                new_directory.borrow_mut().father = Some(Rc::clone(&current_node));
                current_node.borrow_mut().dirs.push(Rc::clone(&new_directory));
                current_node = Rc::clone(&new_directory);
            }
        }
    }

    let total_space = 70000000;
    let space_needed = 30000000;
    let total_used_space = root.borrow().get_dir_size();
    let free_space = total_space - total_used_space;
    let space_to_free = space_needed - free_space;

    let mut all_sizes: Vec<i32> = root.borrow().get_all_size();
    all_sizes.sort();

    let mut size_to_delete = 0;
    for size in all_sizes {
        size_to_delete = size;
        if size >= space_to_free {
            break;
        }
    }
    Ok(size_to_delete.to_string())
}
