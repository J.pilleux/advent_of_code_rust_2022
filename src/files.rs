use anyhow::{anyhow, Result};

fn get_day(day: i32) -> Result<String> {
    if day > 31 || day < 1 {
        Err(anyhow!("Day should be between 1 and 31. Got {}", day))
    } else {
        Ok(format!("{:02}", day))
    }
}

fn get_filename(is_test: bool) -> String {
    match is_test {
        true => String::from("test-input.txt"),
        false => String::from("input.txt"),
    }
}

pub fn get_file_path(day: i32, is_test: bool) -> Result<String> {
    let day = get_day(day)?;
    let filename = get_filename(is_test);

    Ok(format!("src/day_{}/{}", day, filename))
}
