use anyhow::{Result, anyhow};

pub struct Strings {
    first: String,
    second: String,
    third: String,
}

impl Strings {
    pub fn new(s1: &str, s2: &str, s3: &str) -> Strings {
        Strings {
            first: s1.to_string(),
            second: s2.to_string(),
            third: s3.to_string(),
        }
    }

    fn find_duplicate(&self) -> Option<char> {
        for c in self.first.chars() {
            if ! self.second.contains(c) {
                continue;
            }
            if self.third.contains(c) {
                return Some(c);
            }
        }
        None
    }

    fn get_char_score(c: char) -> Result<u8> {
        let u: u8 = c.try_into()?;
        match u.is_ascii_uppercase() {
            true => Ok(u - 38),
            false => Ok(u - 96)
        }
    }

    pub fn get_score(&self) -> Result<u8> {
        match self.find_duplicate() {
            Some(c) => Ok(Strings::get_char_score(c)?),
            None => Err(anyhow!("Duplicate not found"))
        }
    }
}

