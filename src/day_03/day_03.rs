use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::{Result, anyhow};

use super::strings::Strings;

fn day_03_easy(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut score: i32 = 0;

    let mut line_iter = reader.lines().into_iter().peekable();

    while line_iter.peek().is_some() {
        let first = match line_iter.next() {
            Some(res) => match res {
                Ok(l) => l,
                Err(_) => return Err(anyhow!("Cannot parse first line")),
            },
            None => return Err(anyhow!("Cannot get first line")),
        };
        let second = match line_iter.next() {
            Some(res) => match res {
                Ok(l) => l,
                Err(_) => return Err(anyhow!("Cannot parse second line")),
            },
            None => return Err(anyhow!("Cannot get second line")),
        };
        let third = match line_iter.next() {
            Some(res) => match res {
                Ok(l) => l,
                Err(_) => return Err(anyhow!("Cannot parse third line")),
            },
            None => return Err(anyhow!("Cannot get third line")),
        };

        let strs = Strings::new(&first, &second, &third);
        score += strs.get_score()? as i32;
    }

    Ok(score.to_string())
}

pub fn day_03(fpath: String) -> Result<String> {
    Ok(day_03_easy(fpath)?)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day_03_easy() {
        let res = day_03("./src/day_03/test-input.txt".to_string());
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), "157");
    }
}
