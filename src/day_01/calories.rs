pub struct Calories {
    calories: Vec<i32>,
    current: i32,
}

impl Calories {
    pub fn new() -> Calories {
        Calories {
            calories: Vec::new(),
            current: 0,
        }
    }

    pub fn add_cals(&mut self, cals: i32) {
        self.current += cals
    }

    pub fn next(&mut self) {
        self.calories.push(self.current);
        self.current = 0;
    }

    #[allow(dead_code)]
    pub fn get_max(&self) -> Option<&i32> {
        self.calories.iter().max()
    }

    pub fn get_top_3(&mut self) -> i32 {
        self.calories.sort();
        self.calories.iter().rev().take(3).sum()
    }
}
