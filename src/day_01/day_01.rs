use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::Result;

use super::calories::Calories;

pub fn day_01(fpath: String) -> Result<String> {
    let file = File::open(&fpath)?;
    let reader = BufReader::new(file);

    let mut calories = Calories::new();

    for line in reader.lines().into_iter() {
        let l = line?;
        if &l != "" {
            let cals = l.parse::<i32>()?;
            calories.add_cals(cals);
        } else {
            calories.next();
        }
    }

    // match calories.get_max() {
    //     Some(m) => Ok(m.to_string()),
    //     None => Err(anyhow!("Cannot find max calories"))
    // }

    Ok(calories.get_top_3().to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day_01() {
        let res = day_01("./src/day_01/test-input.txt".to_string());
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), "41000");
    }
}
